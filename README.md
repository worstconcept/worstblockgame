# WorstBlockGame

[![ContentDB](https://content.minetest.net/packages/nonchip/worstblockgame/shields/downloads/)](https://content.minetest.net/packages/nonchip/worstblockgame/)


## Requirements

* For playing:
  * [Minetest](https://www.minetest.net/downloads/) 5.3.0
* For building:
  * some modern linux distro (windows people have to use a built archive)
  * `zsh`
  * `git`
  * [moonscript](https://luarocks.org/modules/leafo/moonscript)
  * [Minetest_Game](https://github.com/minetest/minetest_game/releases/tag/5.3.0) 5.3.0

## Building

* first time:
  * clone the repo, `cd` into the folder
  * run `MTG_PATH=/path/to/your/copy/of/minetest_game ./build.sh`
  * symlink the folder into your minetest `games` folder
* update:
  * pull the repo
  * rerun `build.sh` as before


for example, on a recent debian:

```
# first time setup
git clone https://gitlab.com/worstconcept/worstblockgame.git
cd worstblockgame
MTG_PATH=/usr/share/games/minetest/games/minetest_game ./build.sh
mkdir -p ~/.minetest/games
ln -s $(pwd) ~/.minetest/games

# future updates
cd worstblockgame
git pull
MTG_PATH=/usr/share/games/minetest/games/minetest_game ./build.sh
```

## Using a built archive

Instead of building yourself, you can also use a built archive.
This has the benefit of not requiring any of the build dependencies (e.g. if you're on Windows), but the downside of not being updateable as easily.

* Download an archive from our [Releases](https://gitlab.com/worstconcept/worstblockgame/-/releases) page (which are always available), or, if you want in-dev updates, from our [CI Pipelines](https://gitlab.com/worstconcept/worstblockgame/-/pipelines) (those are only available for one month).
* Unpack the archive inside your "games" folder like explained in the [minetest documentation](https://wiki.minetest.net/Games#Installing_games).

**due to the build process, the zip file does *not* contain a single game folder, but its contents. make sure you unpack it into a folder *inside* your "games" folder, not directly into it.**
Essentially you want to end up with this file being `.../minetest/games/wbg/README.md` instead of `.../minetest/games/README.md`.

## Playing

run minetest, select WorstBlockGame, create a map with the following settings:

* Name: *just pick a nice one*
* Seed: *some random number*
* Mapgen: `v7` (all others will be disabled anyway)
* Game: `WorstBlockGame`, duh.

Then play the map.

See our [Wiki](https://gitlab.com/worstconcept/worstblockgame/-/wikis/home) to [get started](https://gitlab.com/worstconcept/worstblockgame/-/wikis/Getting-Started) ingame.
