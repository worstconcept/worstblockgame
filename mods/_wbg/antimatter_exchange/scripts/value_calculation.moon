-- config
MAX_STACK_DEPTH = 64
wbg_ae.value_cache = {}
wbg_ae.const_values = {
  ["group:soil"]: 1
  ["group:sand"]: 1
  ["group:stone"]: 2
  ["group:seed"]: 3
  ["group:sapling"]: 3
  ["group:tree"]: 3
  ["group:leaves"]: 2
  --["group:crop"]: 4 -- group doesn't exist, is queried from `farming` mod below

  ["default:coal_lump"]: 5
  ["default:iron_lump"]: 5
  ["default:copper_lump"]: 5
  ["default:tin_lump"]: 5
  ["default:gold_lump"]: 10
  ["default:mese_crystal"]: 10
  ["default:diamond"]: 10

  ["moreores:mithril_lump"]: 10
  ["moreores:silver_lump"]: 5

  ["elepower_nuclear:uranium_lump"]: 10
  ["elepower_dynamics:lead_lump"]: 5
  ["elepower_dynamics:nickel_lump"]: 5
  ["elepower_dynamics:zinc_lump"]: 5
  ["elepower_dynamics:viridisium_lump"]: 10
}

-- add plants
minetest.after 1, ->
  for k,v in pairs farming.registered_plants
    if v.crop
      wbg_ae.value_cache[v.crop] = 4

walk_recipes = (out_name, fallback, depth, seen)->
  val_min = nil
  recipes = minetest.get_all_craft_recipes(out_name) or {}
  urecipes = unified_inventory.crafts_for.recipe[out_name] or {}
  table.insert recipes, i for i in *urecipes
  return nil unless recipes
  for recipe in *recipes
    continue if recipe.type == 'fuel'
    val_here = 0
    inp = recipe.items
    inp = {inp} unless type(inp)=='table'
    for i in *inp
      i = ItemStack(i)
      continue if i\is_empty!
      iname = i\get_name!
      break unless i\is_known! or string.sub(iname, 1, 6) == "group:"
      incount = i\get_count!
      vnow = wbg_ae.get_value_for iname, fallback, depth, seen
      break if (not vnow) or vnow == fallback or vnow == 0 or vnow == math.huge
      val_here += incount * vnow
    if val_here > 0 and (val_min == nil or val_here < val_min)
      val_min = 1+ (val_here / ItemStack(recipe.output)\get_count!)
  return val_min

wbg_ae.get_value_for = (name, fallback=0, depth = 0, seen_i = {})->
  -- input is invalid
  if name == nil or name == '' or name == 'ignore' or name == 'air'
    return 0

  -- check cache
  return wbg_ae.value_cache[name] if wbg_ae.value_cache[name]

  -- check seen (circular deps)
  seen = {k,true for k,_ in pairs seen_i}
  if seen[name]
    return math.huge

  -- input is group
  if string.sub(name, 1, 6) == "group:"
    group = string.sub(name, 7)
    val_min = nil
    operator = (i)->
      if minetest.get_item_group(i, group) > 0
        val_here = wbg_ae.get_value_for i, fallback, depth+1, seen
        val_min = val_here if val_here >= 1 and (val_min == nil or val_here < val_min)
    for i,_ in pairs minetest.registered_nodes
      operator i
    for i,_ in pairs minetest.registered_craftitems
      operator i
    for i,_ in pairs minetest.registered_tools
      operator i
    if val_min
      wbg_ae.value_cache[name] = val_min
      return val_min
    return fallback

  -- it's an item, so set seen
  seen[name]=true

  -- check const item
  return wbg_ae.const_values[name] if wbg_ae.const_values[name]
  -- check const group
  val_min = nil
  for k,v in pairs wbg_ae.const_values
    if string.sub(k, 1, 6) == "group:"
      group = string.sub(k, 7)
      if minetest.get_item_group(name, group) > 0
        if val_min==nil or v < val_min
          val_min = v
  if val_min
    wbg_ae.value_cache[name] = val_min
    return val_min

  -- check recursion depth
  if depth > MAX_STACK_DEPTH
    return fallback

  -- walk recipes
  recipe_value = walk_recipes name, fallback, depth+1, seen
  if recipe_value
    wbg_ae.value_cache[name] = recipe_value
    return recipe_value

  -- fallback
  minetest.log 'warning', "[WBG:antimatter_exchange] could not find value for #{name}"
  return fallback

