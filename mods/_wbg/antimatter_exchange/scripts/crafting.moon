minetest.register_craft({
  output: "wbg_antimatter_exchange:matter_inverter",
  recipe: {
    {"elepower_dynamics:electrum_plate", "elepower_dynamics:soc", "elepower_dynamics:electrum_plate"},
    {"elepower_machines:resonant_capacitor", "elepower_nuclear:reactor_controller", "elepower_machines:resonant_capacitor"},
    {"elepower_dynamics:viridisium_plate", "elepower_nuclear:reactor_fluid", "elepower_dynamics:viridisium_plate"},
  }
})
minetest.register_craft({
  output: "wbg_antimatter_exchange:antimatter_reverter",
  recipe: {
    {"elepower_dynamics:electrum_plate", "elepower_dynamics:soc", "elepower_dynamics:electrum_plate"},
    {"elepower_machines:resonant_capacitor", "elepower_nuclear:reactor_controller", "elepower_machines:resonant_capacitor"},
    {"elepower_dynamics:viridisium_plate", "elepower_nuclear:reactor_output", "elepower_dynamics:viridisium_plate"},
  }
})
