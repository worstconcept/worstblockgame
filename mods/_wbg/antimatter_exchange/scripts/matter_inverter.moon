get_formspec = (power, input, output)->
  return "size[8,8.5]"..
    default.gui_bg..
    default.gui_bg_img..
    default.gui_slots..
    ele.formspec.power_meter(power)..
    ele.formspec.fluid_bar(1, 0, input)..
    ele.formspec.fluid_bar(7, 0, output)..
    "list[context;src;3,1;1,1;]"..
    "image[5,2;1,1;gui_furnace_arrow_bg.png^[transformR270]"..
    "list[current_player;main;0,4.25;8,1;]"..
    "list[current_player;main;0,5.5;8,3;8]"..
    "listring[current_player;main]"..
    "listring[context;src]"..
    "listring[current_player;main]"..
    default.get_hotbar_bg(0, 4.25)

take_src_item = (list)->
  list_new = {}
  amnt     = 0
  for i,stack in pairs(list) do
    stack\take_item 1
    list_new[i] = stack
    amnt = amnt + 1
  return amnt, list_new

on_timer = (pos, elapsed)->
  meta = minetest.get_meta pos
  inv  = meta\get_inventory!
  in_buffer  = fluid_lib.get_buffer_data pos, "input"
  out_buffer = fluid_lib.get_buffer_data pos, "output"
  input_value = 0

  power = {
    capacity: ele.helpers.get_node_property meta, pos, "capacity"
    storage: ele.helpers.get_node_property meta, pos, "storage"
    usage: ele.helpers.get_node_property meta, pos, "usage"
  }

  power.usage = 0
  meta\set_string "formspec", get_formspec power, in_buffer, out_buffer

  if out_buffer.amount >= out_buffer.capacity
    return true

  src_list = inv\get_list 'src'
  for i, stack in pairs src_list
    input_value = wbg_ae.get_value_for stack\get_name!, 0
    if input_value and input_value > 0
      stack\take_item 1
      src_list[i] = stack
      break

  if (not input_value) and in_buffer.amount >= 1000
    input_value = wbg_ae.get_value_for in_buffer.fluid, 0
    if input_value and input_value > 0
      in_buffer.amount -= 1000
      if in_buffer.amount <= 0
        in_buffer.fluid = ''

  if not input_value or input_value <= 0
    return true

  out_buffer.amount += input_value
  if out_buffer.amount >= out_buffer.capacity
    return true

  power.usage = 128

  if power.storage <= power.usage
    return true

  power.storage -= power.usage


  inv\set_list 'src', src_list
  meta\set_int "input_fluid_storage", in_buffer.amount
  meta\set_string "input_fluid", in_buffer.fluid
  meta\set_int "output_fluid_storage", out_buffer.amount
  meta\set_string "output_fluid", "wbg_antimatter_exchange:antimatter"
  meta\set_int "storage", power.storage
  meta\set_string "formspec", get_formspec power, in_buffer, out_buffer

  return true


ele.register_machine "wbg_antimatter_exchange:matter_inverter", {
  description: "Matter Inverter"
  groups: {oddly_breakable_by_hand: 1, cracky: 1, fluid_container: 1, ele_user: 1, tubedevice: 1, tubedevice_receiver: 1}
  ele_capacity: 12000
  fluid_buffers: {
    input: {
      capacity: 8000
      accepts: true
      drainable: false
    },
    output: {
      capacity: 64000
      accepts: false
      drainable: true
    },
  },
  on_timer: on_timer
  on_construct: (pos)->
    meta = minetest.get_meta pos
    meta\set_string "formspec", get_formspec!
    inv = meta\get_inventory!
    inv\set_size "src", 1
  tiles: {
    "elepower_machine_top.png", "elepower_machine_base.png",
    "elepower_machine_side.png",
  }

  allow_metadata_inventory_put: ele.default.allow_metadata_inventory_put
  allow_metadata_inventory_move: ele.default.allow_metadata_inventory_move
  allow_metadata_inventory_take: ele.default.allow_metadata_inventory_take
  on_metadata_inventory_move: ele.default.metadata_inventory_changed
  on_metadata_inventory_put: ele.default.metadata_inventory_changed
  on_metadata_inventory_take: ele.default.metadata_inventory_changed
}
