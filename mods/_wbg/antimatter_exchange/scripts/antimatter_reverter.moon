get_formspec = (power, input, output)->
  return "size[8,8.5]"..
    default.gui_bg..
    default.gui_bg_img..
    default.gui_slots..
    ele.formspec.power_meter(power)..
    ele.formspec.fluid_bar(1, 0, input)..
    ele.formspec.fluid_bar(7, 0, output)..
    "list[context;src;5,0.5;1,1;]"..
    "list[context;dst;5,1.5;1,1;]"..
    "image[3,2;1,1;gui_furnace_arrow_bg.png^[transformR270]"..
    "list[current_player;main;0,4.25;8,1;]"..
    "list[current_player;main;0,5.5;8,3;8]"..
    "listring[current_player;main]"..
    "listring[context;src]"..
    "listring[current_player;main]"..
    "listring[context;dst]"..
    "listring[current_player;main]"..
    default.get_hotbar_bg(0, 4.25)

on_timer = (pos, elapsed)->
  meta = minetest.get_meta pos
  inv  = meta\get_inventory!
  in_buffer  = fluid_lib.get_buffer_data pos, "input"
  out_buffer = fluid_lib.get_buffer_data pos, "output"
  input_value = 0

  power = {
    capacity: ele.helpers.get_node_property meta, pos, "capacity"
    storage: ele.helpers.get_node_property meta, pos, "storage"
    usage: ele.helpers.get_node_property meta, pos, "usage"
  }

  power.usage = 0
  meta\set_string "formspec", get_formspec power, in_buffer, out_buffer

  if in_buffer.amount <= 0
    return true

  src_stack = inv\get_stack 'src', 1
  src_itm = src_stack\peek_item!

  dst_stack = inv\get_stack 'dst', 1

  if inv\is_empty 'src'
    return true

  src_name = src_itm\get_name!
  out_fluid = false

  for _,v in pairs ele.gases
    if v.itemname==src_name
      src_name = v.source
      out_fluid = true
      break

  for _,v in pairs bucket.liquids
    if v.itemname==src_name
      src_name = v.source
      out_fluid = true
      break

  output_value = wbg_ae.get_value_for src_name, math.huge

  in_buffer.amount -= output_value
  if in_buffer.amount < 0
    return true



  if out_fluid
    out_buffer.amount += 1000
    out_buffer.fluid = src_name
    if out_buffer.amount >= out_buffer.capacity
      return true
  else
    if not dst_stack\item_fits src_itm
      return true
    dst_stack\add_item src_itm

  power.usage = 128

  if power.storage <= power.usage
    return true

  power.storage -= power.usage

  inv\set_stack 'dst', 1, dst_stack
  meta\set_int "input_fluid_storage", in_buffer.amount
  meta\set_string "input_fluid", in_buffer.fluid
  meta\set_int "output_fluid_storage", out_buffer.amount
  meta\set_string "output_fluid", out_buffer.fluid
  meta\set_int "storage", power.storage
  meta\set_string "formspec", get_formspec power, in_buffer, out_buffer

  return true


ele.register_machine "wbg_antimatter_exchange:antimatter_reverter", {
  description: "Antimatter Reverter"
  groups: {oddly_breakable_by_hand: 1, cracky: 1, fluid_container: 1, ele_user: 1, tubedevice: 1}
  ele_capacity: 12000
  fluid_buffers: {
    input: {
      capacity: 64000
      accepts: {'wbg_antimatter_exchange:antimatter'}
      drainable: false
    },
    output: {
      capacity: 8000
      accepts: false
      drainable: true
    },
  },
  on_timer: on_timer
  on_construct: (pos)->
    meta = minetest.get_meta pos
    meta\set_string "formspec", get_formspec!
    inv = meta\get_inventory!
    inv\set_size "src", 1
    inv\set_size "dst", 1
  tiles: {
    "elepower_machine_top.png", "elepower_machine_base.png",
    "elepower_machine_side.png",
  }
}
