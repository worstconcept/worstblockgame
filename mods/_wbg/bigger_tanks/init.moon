export wbg_bigger_tanks = {}
wbg_bigger_tanks.tanks = {}

wbg_bigger_tanks.register_tank_mat = (mat,size, readable=nil)->
  readable = minetest.registered_nodes[mat].description unless readable
  clean = fluid_lib.cleanse_node_name mat
  nodename = "wbg_bigger_tanks:tank_"..clean
  wbg_bigger_tanks.tanks[nodename] = size
  fluid_tanks.register_tank(nodename, {
    description: "Bigger Fluid Tank ("..tostring(size)..' '..readable..")"
    capacity: size
    tiles: minetest.registered_nodes[mat].tiles
    accepts: true
  })

  minetest.register_craft({
    output: nodename,
    recipe: {
      {mat, mat, mat},
      {mat, "fluid_tanks:tank", mat},
      {mat, mat, mat},
    }
  })

wbg_bigger_tanks.register_tank_mat "default:obsidian_glass", 24000
wbg_bigger_tanks.register_tank_mat "elepower_dynamics:hardened_glass", 64000, "Hardened Obsidian Glass"
