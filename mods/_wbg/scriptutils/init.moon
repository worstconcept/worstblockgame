ie = minetest.request_insecure_environment!
debug = ie.require 'debug'

export WSU = {
  registered: (case,name)->
    params = {}
    local list
    switch case
      when "item"
        list = minetest.registered_items
      when "node"
        list = minetest.registered_nodes
      when "craftitem"
        list = minetest.registered_craftitems
      when "tool"
        list = minetest.registered_tools
      when "entity"
        list = minetest.registered_entities
    if list
      for k,v in pairs(list[name])
        params[k] = v
    return params

  get_fn_ups: (fn)->
    ups = {}
    i=1
    while true
      n, v = debug.getupvalue fn, i
      if not n
        break
      ups[n] = v
      i = i + 1
    return ups

  set_fn_up: (fn,upn,upv)->
    i=1
    while true
      n, v = debug.getupvalue fn, i
      if not n
        return false
      if n == upn
        debug.setupvalue fn, i, upv
        return true
      i = i + 1

  doallfiles: (path)->
    for file in *minetest.get_dir_list path, false
      if file\sub(-4) == '.lua'
        dofile path .. "/" .. file
}
