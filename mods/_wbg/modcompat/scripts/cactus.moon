cactus = WSU.registered "node", "default:cactus"
cactus.collision_box={
  type: "fixed",
  fixed: {
       {-0.1875, -0.5, -0.1875, 0.1875, 0.375, 0.1875},
  }
}
cactus.damage_per_second=1
cactus.groups.attached_node=1
cactus.after_dig_node = (pos, node, metadata, digger)->
  default.dig_up(pos, node, digger)
minetest.register_node ":default:cactus", cactus
