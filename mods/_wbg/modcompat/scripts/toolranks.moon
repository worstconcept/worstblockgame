toolranks_extras.register_tool_type "battleaxe", "battle axe", "Swings"
toolranks_extras.register_tool_type "pick", "pickaxe"
toolranks_extras.register_tool_type "billhook", "billhook"
toolranks_extras.register_tool_type "knife", "knife", "Cuts"
toolranks_extras.register_tool_type "bow", "bow", "Shots"
toolranks_extras.register_tool_type "sword", "sword", "Swings"
toolranks_extras.register_tool_type "hoe", "hoe", "Swings"
-- literally generated using: grep register_tool nssm/nssm_materials.lua
-- and search+replace
toolranks.add_tool "nssm:sun_sword"
toolranks.add_tool "nssm:masticone_fang_sword"
toolranks.add_tool "nssm:night_sword"
toolranks.add_tool "nssm:crab_light_mace"
toolranks.add_tool "nssm:crab_heavy_mace"
toolranks.add_tool "nssm:mantis_battleaxe"
toolranks.add_tool "nssm:stoneater_pick"
toolranks.add_tool "nssm:mantis_sword"
toolranks.add_tool "nssm:little_ice_tooth_knife"
toolranks.add_tool "nssm:manticore_spine_knife"
toolranks.add_tool "nssm:felucco_knife"
toolranks.add_tool "nssm:ant_sword"
toolranks.add_tool "nssm:ant_shovel"
toolranks.add_tool "nssm:duck_beak_shovel"
toolranks.add_tool "nssm:mantis_axe"
toolranks.add_tool "nssm:ant_billhook"
toolranks.add_tool "nssm:duck_beak_pick"
toolranks.add_tool "nssm:ant_pick"
toolranks.add_tool "nssm:mantis_pick"
toolranks.add_tool "nssm:tarantula_warhammer"
toolranks.add_tool "nssm:axe_of_pride"
toolranks.add_tool "nssm:gratuitousness_battleaxe"
toolranks.add_tool "nssm:sword_of_eagerness"
toolranks.add_tool "nssm:falchion_of_eagerness"
toolranks.add_tool "nssm:sword_of_envy"
toolranks.add_tool "nssm:sword_of_gluttony"
toolranks.add_tool "nssm:death_scythe"
-- can't use toolranks.add_tool for those because hoes handle their own ranking callback
minetest.override_item "farming:ant_hoe", {
  original_description: "Ant Hoe",
  description: toolranks.create_description "Ant Hoe"}
minetest.override_item "farming:felucco_hoe", {
  original_description: "Felucco Hoe",
  description: toolranks.create_description "Felucco Hoe"}
