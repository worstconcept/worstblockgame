scan_item=(name,list)->
  if list[name]._hunger_ng
    return
  ou = list[name].on_use
  if not ou
    return
  ups = WSU.get_fn_ups ou
  if not ups.hp_change -- that's what the parameter for `item_eat` is called that becomes an upvalue for `on_item_eat`
    return
  minetest.log 'info','[WBG:modcompat] registering scanned fooditem "%s" with hunger_ng'\format name
  heals = math.floor(ups.hp_change/4)
  satiates = math.floor(ups.hp_change)
  if satiates < 0
    heals = math.floor math.min heals*1.5, -1
    satiates = 0 - satiates
  hunger_ng.add_hunger_data name, {
    heals: heals
    satiates: satiates
    returns: ups.replace_with_item
  }

for n,v in pairs minetest.registered_items
  scan_item n, minetest.registered_items

--for n,v in pairs minetest.registered_craftitems
--  scan_item n, minetest.registered_craftitems
--for n,v in pairs minetest.registered_nodes
--  scan_item n, minetest.registered_nodes
--for n,v in pairs minetest.registered_tools
--  scan_item n, minetest.registered_tools
