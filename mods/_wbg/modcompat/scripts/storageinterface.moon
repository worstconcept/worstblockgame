--https://github.com/cx384/storage_interface/issues/8
reg = WSU.registered 'node', 'storage_interface:storage_interface'

fromtube = reg.tube

orig_on_inv_take = reg.on_metadata_inventory_take
update_formspec = WSU.get_fn_ups(orig_on_inv_take).update_formspec
get_storage_table = WSU.get_fn_ups(update_formspec).get_storage_table
get_connected_nodes = WSU.get_fn_ups(get_storage_table).get_connected_nodes

orig_create_changed_pos = WSU.get_fn_ups(get_connected_nodes).create_changed_pos
create_changed_pos = (pos, x, y, z)->
  pos = orig_create_changed_pos pos, x, y, z
  if minetest.get_node(pos).name == 'ignore'
    minetest.log 'info', '[WBG:modcompat] loading %d,%d,%d for storage interface'\format pos.x,pos.y,pos.z
    minetest.get_voxel_manip!\read_from_map pos, pos
  pos
WSU.set_fn_up get_connected_nodes, 'create_changed_pos', create_changed_pos

get_storage_list = (frompos, owner='.ignore_player')->
  nodes = get_connected_nodes(frompos,owner)
  list = {}
  for pos in *nodes
    meta = minetest.get_meta(pos)
    inv = meta\get_inventory()
    for listname, li in pairs(inv\get_lists())
      for stack in *li
        table.insert list, stack
  list


fromtube.return_input_invref = (frompos, fromnode, dir, owner)->
  list = get_storage_list frompos, owner
  return {
    get_list: (name)=> return list
    get_stack: (name,spos)=> return list[spos]
    get_size: (name)=> return #list
  }
fromtube.can_remove = (frompos, fromnode, stack, dir, frominvname, spos)-> return stack\get_count!

--storage_add_item = WSU.get_fn_ups(reg.allow_metadata_inventory_put).storage_add_item
--fromtube.insert_object = (pos, node, stack, vel, owner) ->
--  storage_add_item pos, stack, owner

storage_remove_item = WSU.get_fn_ups(reg.allow_metadata_inventory_take).storage_remove_item
fromtube.remove_items = (frompos, fromnode, stack, dir, count, frominvname, spos)->
  filtpos = vector.add(frompos, dir)
  filtmeta = minetest.get_meta(filtpos)
  filtinv = filtmeta\get_inventory()
  owner = filtmeta\get_string("owner")
  exmatch = filtmeta\get_int("exmatch_mode")
  stack\set_count count
  return storage_remove_item(frompos, stack, not (exmatch==0 or exmatch ==nil), owner)


--https://github.com/cx384/storage_interface/issues/9

orig_str_find = string.find
string.find = (...)->
  args = {...}
  ret = {pcall (-> orig_str_find unpack args)}
  return unpack ret,2

-- https://gitlab.com/worstconcept/worstblockgame/-/issues/14
match_filter = WSU.get_fn_ups(update_formspec).match_filter

reg.digiline = {
    receptor: {}
    effector: {
      action: (pos, node, channel, msg)->
        meta = minetest.get_meta pos
        return unless channel == meta\get_string 'infotext'
        list=get_storage_list pos
        filter = ''
        if type(msg) == 'table'
          first = true
          for str in *msg
            if string.sub(str, 1, 6) ~= "group:"
              str = '"'..str..'"'
            if not first
              str = ','..str
            filter ..= str
            first = false
        elseif type(msg) == 'string'
          filter = msg
        results = {}
        for item in *list
          name=item\get_name!
          if match_filter name, filter
            results[name] or= 0
            results[name] += item\get_count!
        digilines.receptor_send(pos, digilines.rules.default, channel, results)
  }
}
minetest.register_node ':storage_interface:storage_interface', reg


-- seriously use gothdamn public APIs, look what you're making me do here!!!
sort_storage = nil
have_instrumentation = false
for f in *minetest.registered_on_player_receive_fields
  serialized = minetest.serialize f
  if string.find(serialized, 'builtin/profiler/instrumentation')
    have_instrumentation = true
    break
  if string.find(serialized, "storage_interface:storage_interface") and string.find(serialized, "sort_storage")
    ups = WSU.get_fn_ups(f)
    if ups.sort_storage
      sort_storage = ups.sort_storage
      break

if sort_storage
  minetest.register_abm {
    label: 'WBG Storage Interface Sorting'
    nodenames: {"storage_interface:storage_interface"}
    neighbors: {'default:diamondblock'}
    interval: 60
    chance: 60
    catch_up: false
    action: (pos)->
      minetest.log 'action', "[WBG:modcompat] sorting storage interface at #{pos.x},#{pos.y},#{pos.z}"
      sort_storage pos, ".ignore_player"
  }
  minetest.log 'info', '[WBG:modcompat] set up storage interface sorting'
else
  minetest.log 'warning', '[WBG:modcompat] could not set up storage interface sorting'.. (have_instrumentation and ', disable instrumentation!' or '')
