aliases = {
  ['farming:blueberries']:  { 'default:blueberries' }
  ['bbq:sea_salt']: { 'farming:salt' }
  ['ethereal:bowl']: { 'farming:bowl' }
  ['elepower_dynamics:wood_dust']: { 'bbq:sawdust' }
}

for master,items in pairs aliases
  for item in *items
    minetest.log 'info','[WBG:modcompat] unifying %s to %s'\format item, master
    minetest.register_alias_force item, master
