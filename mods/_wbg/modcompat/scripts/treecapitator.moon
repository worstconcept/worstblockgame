treecapitator.register_tree{
  trees: {"ethereal:sakura_trunk"}
  leaves: {"ethereal:sakura_leaves", "ethereal:sakura_leaves2"}
  fruits: {"ethereal:sakura_trunk"}
  trunk_fruit_vertical: true
  range: 5
  stem_height_min: 5
  type: "default"
}
