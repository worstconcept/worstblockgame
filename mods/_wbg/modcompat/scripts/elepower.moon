-- because the elepower nuclear stuff is using "max_stack" which doesn't exist
fix_stack_max = (name)->
  item = WSU.registered "craftitem", name
  item.stack_max=1
  minetest.register_craftitem ":"..name, item

fix_stack_max 'elepower_nuclear:fuel_rod_fissile'
fix_stack_max 'elepower_nuclear:fuel_rod_depleted'

port = WSU.registered 'node', "elepower_nuclear:reactor_power"
old_port_timer = port.on_timer
if true
  port.on_timer = (...)->
    old_port_timer ...
    return true
  minetest.register_node ":elepower_nuclear:reactor_power", port

ctrl = WSU.registered 'node', "elepower_nuclear:reactor_controller"
old_ctrl_timer = ctrl.on_timer
if true
  ctrl.on_timer = (...)->
    old_ctrl_timer ...
    return true
  minetest.register_node ":elepower_nuclear:reactor_controller", ctrl

get_fusion_recipe = WSU.get_fn_ups(old_ctrl_timer).get_recipe
reactions = WSU.get_fn_ups(get_fusion_recipe).reactions
reactions[1].time = 10
reactions[1].power_ignite = 1000
reactions[1].power_upkeep = 500
reactions[1].output = "elepower_nuclear:helium_plasma 8000"

eva = WSU.registered 'node', 'elepower_thermal:evaporator_controller'
get_eva_recipe = WSU.get_fn_ups(eva.on_timer).get_recipe
recipes = WSU.get_fn_ups(get_eva_recipe).results
recipes[1].output = "elepower_thermal:brine_source 500"
recipes[2].output = "elepower_dynamics:lithium_source 500"

exc = WSU.registered 'node', 'elepower_nuclear:heat_exchanger'
heat_recipes = WSU.get_fn_ups(exc.on_timer).heat_recipes
heat_recipes["elepower_nuclear:helium_plasma"].factor = 64

local net_abm
for abm in *minetest.registered_abms
  if abm.label=='elepower Power Transfer Tick'
    net_abm = abm
    break
assert(net_abm, 'could not find elepower transfer ABM')
abm_ups = WSU.get_fn_ups(net_abm.action)
orig_discover_branches = abm_ups.discover_branches
-- true: a before b
sort_provider=(a,b)->
  ameta = minetest.get_meta a
  bmeta = minetest.get_meta b
  acap = ele.helpers.get_node_property(ameta, a, "capacity") or 1
  bcap = ele.helpers.get_node_property(bmeta, b, "capacity") or 1
  astor = ameta\get_int("storage") or 1
  bstor = bmeta\get_int("storage") or 1
  astor /= acap
  bstor /= bcap
  return astor > bstor
sort_user=(a,b)->
  ameta = minetest.get_meta a
  bmeta = minetest.get_meta b
  acap = ele.helpers.get_node_property(ameta, a, "capacity") or 1
  bcap = ele.helpers.get_node_property(bmeta, b, "capacity") or 1
  astor = ameta\get_int("storage") or 1
  bstor = bmeta\get_int("storage") or 1
  astor /= acap
  bstor /= bcap
  return astor < bstor

discover_branches_cache = {}
discover_branches = (pr_pos, positions)->
  hpos = minetest.hash_node_position
  if discover_branches_cache[hpos] and (math.random(10)==1 or discover_branches_cache[hpos].time >= minetest.get_us_time! )
    return unpack discover_branches_cache[hpos].value
  users, providers = orig_discover_branches(pr_pos,positions)
  nbat_users={}
  nbat_providers={}
  bat_users={}
  bat_providers={}
  for spos in *users
    node = minetest.registered_nodes[minetest.get_node(spos).name]
    if minetest.get_item_group(node.name, "ele_storage") > 0
      table.insert bat_users, spos
    else
      table.insert nbat_users, spos
  for spos in *providers
    node = minetest.registered_nodes[minetest.get_node(spos).name]
    if minetest.get_item_group(node.name, "ele_storage") > 0
      table.insert bat_providers, spos
    else
      table.insert nbat_providers, spos

  table.sort nbat_providers, sort_provider
  table.sort bat_providers, sort_provider
  table.sort nbat_users, sort_user
  table.sort bat_users, sort_user

  users = {}
  providers = {}
  for i in *nbat_providers
    table.insert providers, i
  for i in *bat_providers
    table.insert providers, i
  for i in *nbat_users
    table.insert users, i
  for i in *bat_users
    table.insert users, i
  discover_branches_cache[hpos] = {time: minetest.get_us_time!+((math.random 5,25)*100000), value: {users, providers}}
  return unpack discover_branches_cache[hpos].value

orig_clear_networks = ele.clear_networks
ele.clear_networks = (pos)->
  orig_clear_networks pos
  discover_branches_cache = {}

WSU.set_fn_up net_abm.action, 'discover_branches', discover_branches
