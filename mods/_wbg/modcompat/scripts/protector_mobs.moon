for name, ent in pairs minetest.registered_entities
  continue unless ent.type=='monster'
  minetest.log 'info','[WBG:modcompat] overriding mob %s to suicide in protected areas'\format name
  orig_custom = ent.do_custom
  ent.do_custom = (dtime)=>
    pos = @object\get_pos!
    if minetest.is_protected pos, ''
      mobs\remove @, true
      return false
    else
      if orig_custom
        return orig_custom @, dtime
      else
        return true
