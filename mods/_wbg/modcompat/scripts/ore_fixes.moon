for name, oredef in pairs minetest.registered_ores
  if oredef.ore_type=='scatter' and oredef.wherein=='default:stone' and oredef.y_min < 0
    minetest.log 'info','[WBG:modcompat] forcing ore y_min from %d to endless for %s'\format oredef.y_min, (type(name)=='string' and name or oredef.ore)
    oredef.y_min = -31000

  if oredef.ore=='elepower_nuclear:stone_with_uranium' and oredef.y_max < -1000
    minetest.log 'info', '[WBG:modcompat] explicit ore override for low-y uranium'
    oredef.clust_scarcity = 10 * 10 * 10
    oredef.clust_size = 6
    oredef.clust_num_ores = 10
