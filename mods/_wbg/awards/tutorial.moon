{
  {
    title: 'You know the drill.'
    description: 'Punch a Tree'
    icon: "awards_first_day_in_the_woods.png"
    difficulty: 0.00001
    trigger: {
      type: "dig"
      node: "group:tree"
      target: 1
    }
  },
  {
    title: 'Sticky Situation'
    description: 'Even more efficient than the competition!'
    icon: "awards_first_day_in_the_woods.png"
    difficulty: 0.0001
    trigger: {
      type: "craft"
      item: "group:stick"
      target: 1
    }
  },
  {
    title: 'Diggy Diggy Hole!'
    description: 'I am a dwarf and I\'m digging a pickaxe...'
    icon: "awards_first_day_in_the_woods.png"
    difficulty: 0.001
    trigger: {
      type: "craft"
      item: "group:pickaxe"
      target: 1
    }
  },
}
