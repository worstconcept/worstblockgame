{
  award_lightitup: {
    'award_well_lit',
    'award_meselamp'
  }
  awards_stonebrick: awards_stonebrick2: 'awards_stonebrick3'
  awards_obsidian: award_obsessed_with_obsidian: 'award_obsidianbrick'
  award_lumberjack_firstday: award_lumberjack: award_lumberjack_semipro: 'award_lumberjack_professional'
  award_junglebaby: 'award_jungleman'
  award_mesefind: 'award_meseblock'
  award_mine2: award_mine3: 'award_mine4'
  awards_gold_ore: 'awards_gold_rush'
  awards_diamond_ore: {
    'awards_diamond_rush',
    'awards_diamondblock'
  }
  award_chest: 'award_chest2'
  awards_farmer: awards_farmer2: awards_farmer3: 'awards_farmer4'
  awards_brown_mushroom1: awards_brown_mushroom2: 'awards_brown_mushroom3'
  awards_builder1: awards_builder2: awards_builder3: 'awards_builder4'
}
