main_pt = dofile minetest.get_modpath('wbg_awards')..'/progression_tree.lua'
tut_awards = dofile minetest.get_modpath('wbg_awards')..'/tutorial.lua'

unregister_award = (name)->
  if not awards.registered_awards[name]
    return nil, "not found"
  minetest.log "info","[WBG:awards] unregistering award \"%s\""\format name
  awards.registered_awards[name]=nil
  return true

add_requires = (name,other)->
  if not awards.registered_awards[name]
    return nil, "not found"
  if not awards.registered_awards[name].requires
    awards.registered_awards[name].requires = {}
  minetest.log "info","[WBG:awards] adding requirement \"%s\" for \"%s\""\format other, name
  table.insert awards.registered_awards[name].requires, other
  return true

add_progression = (...)->
  list = {...}
  for i, name in ipairs list
    if i==1
      continue
    add_requires name, list[i-1]

add_progression_tree = (tree,parent=nil)->
  if type(tree)=='table'
    for k,v in pairs(tree)
      name = k
      child = v
      if type(name)=='number'
        add_progression_tree child, parent
      else
        if parent
          add_requires name, parent
        add_progression_tree child, name
  else if tree and parent
    add_requires tree, parent

filter_invalid_awards=->
  for name,award in pairs awards.registered_awards
    if not award.trigger
      continue
    if award.trigger.item and not minetest.registered_items[award.trigger.item]
      minetest.log "info","[WBG:awards] missing award item \"%s\""\format award.trigger.item
      unregister_award name
    if award.trigger.node and not minetest.registered_nodes[award.trigger.node]
      minetest.log "info","[WBG:awards] missing award node \"%s\""\format award.trigger.node
      unregister_award name

minetest.after 1, ->
  filter_invalid_awards!
  for name,award in pairs awards.registered_awards
    add_requires name, 'wbg_tutorial_'..#tut_awards
  for i, award in ipairs tut_awards
    awards.register_award 'wbg_tutorial_'..i, award
    if i > 1
      add_requires 'wbg_tutorial_'..i, 'wbg_tutorial_'..(i-1)
  add_progression_tree main_pt
