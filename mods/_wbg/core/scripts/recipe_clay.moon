minetest.register_craft {
  type: "shapeless"
  output: "default:clay"
  recipe: {
    "group:sand",
    "bucket:bucket_water"
  }
  replacements: {
    {"bucket:bucket_water", "bucket:bucket_empty"}
  }
}
