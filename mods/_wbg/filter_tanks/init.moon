register_filtertank = (i,basetank="fluid_tanks:tank",size=8000)->
  name="wbg_filter_tanks:tank_".. (size ~= 8000 and (tostring(size)..'_') or '' ) .. fluid_lib.cleanse_node_name i.source
  fluid_tanks.register_tank(name, {
    description: if size==8000 then
      "Filter Tank ("..minetest.registered_nodes[i.source].description..")"
    else
      "Bigger Filter Tank ("..tostring(size)..' '..minetest.registered_nodes[i.source].description..")"
    capacity: size,
    tiles: minetest.registered_nodes[basetank].tiles
    accepts: i.source,
  })
  if i.itemname
    minetest.register_craft {
      output: name,
      type: "shapeless"
      recipe: {basetank,i.itemname}
    }

wbg_bigger_tanks.tanks
for _,i in pairs bucket.liquids
  register_filtertank i
  for name,size in pairs wbg_bigger_tanks.tanks
    register_filtertank i, name, size
for _,i in pairs ele.gases
  register_filtertank i
  for name,size in pairs wbg_bigger_tanks.tanks
    register_filtertank i, name, size

for _,i in pairs minetest.registered_nodes
  if string.sub(i.name,1,22)=="wbg_filter_tanks:tank_"
    i.groups.not_in_craft_guide=1
