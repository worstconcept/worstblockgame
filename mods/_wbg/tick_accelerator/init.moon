minetest.register_abm {
  label: 'WBG Tick Accelerator'
  nodenames: {"elepower_dynamics:viridisium_block"}
  interval: 1
  chance: 1
  catch_up: false
  action: (pos, node, active_object_count, active_object_count_wider)->
    for x = -2, 2
      for y = -2, 2
        for z = -2, 2
          neighpos = vector.add pos, {:x,:y,:z}
          thing = minetest.get_node neighpos
          if thing.name == "elepower_dynamics:viridisium_block"
            continue
          for abm in *minetest.registered_abms
            continue if abm.label and abm.label\sub(-9) == ' spawning' -- because mobs go crazy otherwise
            is_abm = false
            for name in *abm.nodenames
              if thing.name == name
                is_abm = true
                break
            continue if not is_abm
            if (not abm.neighbors) or #abm.neighbors == 0 or minetest.find_node_near neighpos, 1, abm.neighbors, false
              abm.action neighpos, thing, active_object_count, active_object_count_wider
          timer = minetest.get_node_timer neighpos
          ont = minetest.registered_nodes[thing.name].on_timer
          if timer and ont and timer\is_started!
            ont neighpos, timer\get_timeout!
}
