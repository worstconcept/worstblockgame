#!/bin/zsh
setopt extended_glob

mtgpath=${MTG_PATH:-"$HOME/src/minetest_game-5.3.0"}
cpy_mtg(){
  cp -r "${mtgpath}/mods/${1}" "mods/_mtg/${1}"
}

git submodule update --init --recursive


find mods/_wbg -name "*.lua" -type f -delete
moonc mods/_wbg

rm -r mods/_mtg/^(modpack.txt|.gitignore)

# APIS
cpy_mtg player_api
cpy_mtg sfinv

# basegame mods
cpy_mtg default  # default core stuff (things like stone)
cpy_mtg creative # creative mode menu
cpy_mtg dye      # dye system
cpy_mtg map      # minimap
cpy_mtg flowers
#cpy_mtg farming # overridden by farming redo!
cpy_mtg stairs
cpy_mtg tnt
cpy_mtg vessels
cpy_mtg wool
cpy_mtg fire
cpy_mtg beds
#cpy_mtg bucket # overridden by elepower
cpy_mtg doors
cpy_mtg screwdriver
cpy_mtg sethome
cpy_mtg dungeon_loot
cpy_mtg boats
cpy_mtg env_sounds
cpy_mtg fireflies
cpy_mtg bones


for file in $(find overrides -type f)
  cp $file ${file#overrides/}
